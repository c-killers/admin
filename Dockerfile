### BUILDER
FROM tiangolo/meinheld-gunicorn:python3.7-alpine3.8 as builder

LABEL maintainer="C-Real Killers <c-realkillers@digitalonus.com>"

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev git

COPY requirements/ /tmp/requirements

RUN pip install --upgrade pip && pip install -r /tmp/requirements/prod.txt

RUN pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r /tmp/requirements/prod.txt

### MAIN
FROM tiangolo/meinheld-gunicorn:python3.7-alpine3.8

RUN apk update && apk add libpq
COPY --from=builder /usr/src/app/wheels /wheels
COPY --from=builder /tmp/requirements/prod.txt /tmp/requirements/prod.txt
RUN pip install --upgrade pip
RUN pip install --no-cache /wheels/*

COPY ./ /app

COPY prestart.sh /app/prestart.sh

ENV MODULE_NAME AdminKiller.wsgi

ENV VARIABLE_NAME application


#ENV APP_MODULE AdminKiller

#ENV SQLALCHEMY_DATABASE_URL "sqlite:////ckillerdb"

"""
Django settings for AdminKiller project.

Generated by 'django-admin startproject' using Django 2.2.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
from pathlib import Path

from django.urls.base import reverse_lazy

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'local-test-secret-key'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django_admin_bootstrapped',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'rest_framework',
    'ckillers.apps.CkillersAdminConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'AdminKiller.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'AdminKiller.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'ckillerdb',
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/files/'

AUTH_USER_MODEL = 'ckillers.Client'
LOGIN_URL = reverse_lazy('admin:login')

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated'
    ]
}

CORS_ORIGIN_WHITELIST = [
    'http://localhost:3000'
]

API_URL = 'http://localhost:8002/api'
PIPELINES_ROOTDIR = 'files'

from ckillers.gitlab_parser import GitlabCIParser

TYPES_CHOICES = (
    (1, "Jenkins"),
    (2, "Gitlab CI"),
    (3, "Circle CI"),
    (4, "TFE Build"),
    (5, "Other"),
)

IGNORE_PIPELINE_CODE = [ 4 ]

DEFAULT_STAGES_BY_TYPE = {
    4: ('created', 'planning', 'approval', 'applying')
}

PIPELINE_STATUS_OPTIONS = [
    ('running','Running'),
    ('pending','Pending'),
    ('success','Success'),
    ('failed','Failed'),
    ('failure','Failure'),
    ('cancelled','Cancelled'),
    ('skipped','Skipped'),
    ('canceled','Cancelled'),
    ('applied','Applied')
]

PIPELINE_IDS = {
    'jenkins': 'BUILD_ID',
    'gitlab': 'CI_PIPELINE_ID',
    'circle': 'CIRCLE_BUILD_ID',
    'other': 'OTHER_BUILD_ID',
}

FILE_EXTENSIONS = {
    'gitlab': 'yml',
    'jenkins': 'Jenkinsfile',
    'circle': 'yml',
    'tfe': 'tf',
    'other': 'txt',
}

CI_TOOLS = {
    2: GitlabCIParser,
}

CLIENT_PERMISSIONS_OVER_MODELS = ('stage', 'event', 'pipeline', 'project')

CLIENT_VIEWONLY_PERMISSIONS_OVER_MODELS = ('pipelinemessage',)

try:
    from .local_settings import *
except ImportError:
    pass

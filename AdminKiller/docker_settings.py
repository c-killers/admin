from .settings import *

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
SECRET_KEY = os.environ.get('SECRET_KEY','XXXXX')

DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
STATICFILES_STORAGE = 'AdminKiller.gcloud_storage.GoogleCloudStaticStorage'

GS_BUCKET_NAME = 'douhackaton-storage'

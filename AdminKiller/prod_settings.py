from .settings import *
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
DATABASES = {
     'default': {
         'ENGINE': 'django.db.backends.postgresql',
         'HOST': os.environ.get('DB_HOST'),
         'USER': os.environ.get('DB_USER','postgres'),
         'PASSWORD': os.environ.get('DB_PASSWORD'),
         'NAME': os.environ.get('DB_NAME'),
     }
}
SECRET_KEY = os.environ.get('SECRET_KEY', 'XXXXX')
API_URL = 'https://c-realkillers.site/api'
ALLOWED_HOSTS = ['c-realkillers.site']
USE_X_FORWARDED_HOST = True
FORCE_SCRIPT_NAME = '/admin'

DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
STATICFILES_STORAGE = 'AdminKiller.gcloud_storage.GoogleCloudStaticStorage'
GS_BUCKET_NAME = 'douhackaton-storage'

DEBUG = os.environ.get('DEBUG', 'false').lower() == 'true'

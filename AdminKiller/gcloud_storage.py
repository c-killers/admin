"""
GoogleCloudStorage extension for handing Django's Static files.
"""
from django.conf import settings
from storages.backends.gcloud import GoogleCloudStorage
from urllib.parse import urljoin


class GoogleCloudStaticStorage(GoogleCloudStorage):
    """GoogleCloudStorage suitable for Django's Static files"""

    def __init__(self, *args, **kwargs):
        """settings.STATIC_URL is expected to start with /"""
        if not settings.STATIC_URL:
            raise Exception('STATIC_URL has not been configured')
        super().__init__(*args, **kwargs)
        self.prefix = settings.STATIC_URL

    def url(self, name):
        """.url that doesn't call Google. settings.STATIC_URL is expected to start with /"""
        return urljoin(
            f'http://storage.googleapis.com/{self.bucket_name}{settings.STATIC_URL}', name)

    def save(self, prefixed_path, source_file):
        super().save(self.prefix + prefixed_path, source_file)

"""AdminKiller URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from rest_framework_simplejwt import views as jwt_views

from ckillers import views as admin_views
from ckillers.viewsets import ClientView
from ckillers.routers import router

urlpatterns = [
    path('graphs/', include('ckillers.urls')),
    path('', admin.site.urls, name='index'),
    path('api/', include(router.urls)),
    path('api/me', ClientView.as_view(), name='client'),
    path('api/token', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh', jwt_views.TokenRefreshView.as_view(), name='token_refresh_view'),
]

handler404 = admin_views.error_404
handler500 = admin_views.error_500

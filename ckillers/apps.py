from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.conf import settings


def add_permissions(group, permissions, ro_permissions):
    for permission in ro_permissions:
        group.permissions.add(permission)
    return [
        (
            group.permissions.add(view),
            group.permissions.add(add),
            group.permissions.add(change),
            group.permissions.add(delete),
        ) for view, add, change, delete in permissions
    ]


def define_ckillers_groups(sender, **kwargs):
    from django.contrib.auth.models import Group, Permission
    client_permissions = [
        (
            Permission.objects.get(codename=f'view_{model}'),
            Permission.objects.get(codename=f'add_{model}'),
            Permission.objects.get(codename=f'change_{model}'),
            Permission.objects.get(codename=f'delete_{model}')
        ) for model in settings.CLIENT_PERMISSIONS_OVER_MODELS
    ]

    client_ro_permissions = [
        Permission.objects.get(codename=f'view_{model}')
        for model in settings.CLIENT_VIEWONLY_PERMISSIONS_OVER_MODELS
    ]
    group, _ = Group.objects.get_or_create(name='clients')
    add_permissions(group, client_permissions, client_ro_permissions)


class CkillersAdminConfig(AppConfig):
    name = 'ckillers'
    verbose_name = 'Ckillers Admin app'

    def ready(self):
        post_migrate.connect(define_ckillers_groups, sender=self)

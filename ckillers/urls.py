from django.urls import path

from .views import graphs_list, StageTimeGraph, JobTimeGraph, PipeStatusGraph

urlpatterns = [
    path('stage_time_graph/<slug:slug>', StageTimeGraph.as_view(), name='stage_time'),
    path('job_time_graph/<slug:slug>', JobTimeGraph.as_view(), name='job_time'),
    path('pipeline_status_graph/<slug:slug>', PipeStatusGraph.as_view(), name='pipe_status'),
    path('<type>', graphs_list, name='graphs'),
]

from pathlib import Path

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser
from django.conf import settings

import yaml

from .utils import unique_slug, replace_file

from secrets import token_hex


class SlugMixinSave:
    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = unique_slug(self.name, self.__class__)
        super().save(*args, **kwargs)


class SlugWithMixinSave:
    @staticmethod
    def set_uri(obj):
        if isinstance(obj,Pipeline) and int(obj.type) in settings.IGNORE_PIPELINE_CODE:
            return f'{obj._prefix}/{obj.slug}?TOKEN='
        elif isinstance(obj,Stage) and int(obj.pipeline.type) in settings.IGNORE_PIPELINE_CODE:
            return f'{obj._prefix}/{obj.pipeline._tool}/{obj.pipeline.slug}?TOKEN='
        else:
            return f'{obj._prefix}/{obj.slug}'

    def save(self, **kwargs):
        if not self.id:
            self.slug = unique_slug(self.name, self.__class__)
            self.uri = SlugWithMixinSave.set_uri(self)

        super().save()


class Client(AbstractUser):
    timezone = models.CharField(max_length=5)
    token = models.CharField(max_length=50, unique=True, null=True)

    def __str__(self):
        return self.username

    def generate_token(self):
        token = token_hex(16)
        while Client.objects.filter(token=token).exists():
            token = token_hex(16)
        return token

    def save(self, *args, **kwargs):
        if not self.id:
            self.token = self.generate_token()
        super().save(*args, **kwargs)


class Project(SlugMixinSave, models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=150, unique=True, null=True)

    def __str__(self):
        return "{} - {}".format(self.client, self.name)


class Pipeline(SlugWithMixinSave, models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=20)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    slug = models.SlugField(max_length=150, unique=True, null=True)
    uri = models.URLField(null=True)

    @property
    def _tool(self):
        return dict(settings.TYPES_CHOICES)[int(self.type)].split()[0].lower()

    def get_pipeline_file_name(self):
        return f'{self.slug}.{settings.FILE_EXTENSIONS[self._tool]}'

    @property
    def _prefix(self):
        if int(self.type) in settings.IGNORE_PIPELINE_CODE:
            return f'/message/{self._tool}'
        else:
            return f'/message/{self._tool}/pipeline'

    def __str__(self):
        return self.name

    def get_pipe_image(self):
        type_dict = {
            '2': 'gitlab.png',
            '1': 'jenkins.png',
            '4': 'terraform.png',
            '5': 'other.png'
        }
        if self.type in type_dict:
            return type_dict[self.type]
        return type_dict[5]


class Stage(SlugWithMixinSave, models.Model):
    name = models.CharField(max_length=100)
    pipeline = models.ForeignKey(Pipeline, on_delete=models.CASCADE)
    uri = models.URLField(null=True)
    slug = models.SlugField(max_length=150, unique=True, null=True)
    _prefix = '/events'

    @property
    def _tool(self):
        return dict(settings.TYPES_CHOICES)[int(self.pipeline.type)].split()[0].lower()

    def __str__(self):
        return "{} - {}".format(self.pipeline, self.name)


class Event(models.Model):
    stage = models.ForeignKey(Stage, on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField(null=True, blank=True)
    job_id = models.CharField(max_length=100, default=0)

    def __str__(self):
        return "{} - {}/{}".format(self.stage, self.start, self.end)


class PipelineMessage(models.Model):
    pipeline = models.ForeignKey(Pipeline, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    message = models.CharField(max_length=100)
    job_id = models.CharField(max_length=100, default=0)
    status = models.CharField(max_length=50, choices=settings.PIPELINE_STATUS_OPTIONS, default='success' )
    metadata = models.TextField(null=True, blank=True)

    def __str__(self):
        return "{} - {}".format(self.pipeline, self.message)


@receiver(post_save, sender=Pipeline, dispatch_uid="create_stages_from_pipeline_code")
def create_stages(sender, instance, **kwargs):
    _type = int(instance.type)
    
    if not hasattr(instance, "_pipeline_code") and _type not in settings.IGNORE_PIPELINE_CODE:
        return
    
    ParserClass = settings.CI_TOOLS.get(_type)
    if not ParserClass and _type not in settings.IGNORE_PIPELINE_CODE:
        raise NotImplementedError(f'{dict(settings.TYPES_CHOICES)[_type]} parser is not implemented yet')

    if _type not in settings.IGNORE_PIPELINE_CODE:
        pipeline_code = getattr(instance, '_pipeline_code', {})
        parser = ParserClass(pipeline_code)
        job_names = parser.job_names
    else:
        job_names = settings.DEFAULT_STAGES_BY_TYPE[_type]

    Stage.objects.filter(pipeline=instance).delete()
    uris = {
        name: Stage.objects.create(name=name, pipeline=instance).uri for name in job_names
    }

    if _type not in settings.IGNORE_PIPELINE_CODE:
        iac_code = parser.parse(uris, instance.project.client.token)
        file_name = f'{settings.PIPELINES_ROOTDIR}/{instance.get_pipeline_file_name()}'
        replace_file(file_name, iac_code)

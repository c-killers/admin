# Generated by Django 2.2.7 on 2019-11-27 23:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ckillers', '0005_remove_event_slug'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='token',
            field=models.CharField(max_length=50, null=True),
        ),
    ]

from rest_framework import serializers

from ckillers.models import *


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        exclude = ('id',)

class PipelineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pipeline
        exclude = ('id',)


class StageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stage
        exclude = ('id',)


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        exclude = ('id',)


class PipelineMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PipelineMessage
        exclude = ('id',)

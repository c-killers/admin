from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import DjangoModelPermissions

from .models import *
from .serializers import *


class ClientView(APIView):
    def get(self, request):
        content = {
            'username': request.user.username,
            'token': request.user.token,
            'email': request.user.email,
        }
        return Response(content)


class ProjectViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissions]
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class PipelineViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissions]
    queryset = Pipeline.objects.all()
    serializer_class = PipelineSerializer


class StageViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissions]
    queryset = Stage.objects.all()
    serializer_class = StageSerializer


class EventViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissions]
    queryset = Event.objects.all()
    serializer_class = EventSerializer


class PipelineMessageViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissions]
    queryset = PipelineMessage.objects.all()
    serializer_class = PipelineMessageSerializer

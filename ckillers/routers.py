from rest_framework.routers import DefaultRouter

from ckillers.viewsets import *

router = DefaultRouter()
router.register('projects', ProjectViewSet)
router.register('pipelines', PipelineViewSet)
router.register('stages', StageViewSet)
router.register('events', EventViewSet)
router.register('pipelinemessages', PipelineMessageViewSet)

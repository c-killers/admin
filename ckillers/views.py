from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView
from django.http import Http404
from django.db.models import Count


from .models import Pipeline, Event, PipelineMessage

from datetime import datetime, timedelta


from pytz import UTC

TIMES = {
    "1": {'hours': 1, 'text': "One Hour"},
    "2": {'hours': 336, 'text': "Two weeks"},
    "3": {'hours': 730, 'text': "One month"},
    "4": {'hours': 1460, 'text': "Two months"},
    "5": {'hours': 4380, 'text': "six months"}
}

GRAPH_TYPES = {
    'job_time': 'Job time graph',
    'stage_time': "Stage time graph",
    'pipe_status': "Pipeline status graph"
}


def belongs_to_clients_group(user):
    return user.is_superuser or 'clients' in [g.name for g in user.groups.all()]


@user_passes_test(belongs_to_clients_group)
@login_required()
def graphs_list(request, type):
    if type in GRAPH_TYPES:
        superuser = request.user.is_superuser
        pipelines = Pipeline.objects.all() if superuser else Pipeline.objects.filter(project__client=request.user)
        context = {
            'title': GRAPH_TYPES[type],
            'results': pipelines,
            'site_header': "CKiller Admin",
            'has_permission': True,
            'type': type
        }
        return render(request, 'ckillers/graphs_list.html', context)
    raise Http404()


class GraphView(LoginRequiredMixin, TemplateView):
    labels = []
    data = []
    pipeline = None
    graph_title = "Graph title"
    template_name = 'ckillers/index_graph.html'

    def set_time(self, request):
        self.time_option = '5'
        self.time_object = TIMES[self.time_option]
        if 'time' in request.GET:
            self.time_option = request.GET['time']
            self.time_object = TIMES[request.GET['time']]

    def get_context_data(self):
        return {
            'title': self.graph_title,
            'has_permission': True,
            'labels': self.labels,
            'site_header': "CKiller Admin",
            'data': self.data,
            'pipeline': self.pipeline,
            'times': TIMES,
            'time_selected': self.time_option,
            'type': self.type,
            'units': self.units
        }

    def get(self, request, slug):
        self.set_time(request)
        self.get_graph_data(slug)
        return super().get(request, slug)


class StageTimeGraph(GraphView):
    graph_title = "Stage times graph"
    type = "stage_time"
    units = "mins"

    def get_graph_data(self, slug):
        self.labels, self.data = [], []
        self.pipeline = get_object_or_404(Pipeline, slug=slug)
        for stage in self.pipeline.stage_set.all():
            date_request = datetime.now(tz=UTC) - timedelta(hours=self.time_object['hours'])
            valid_events = [event for event in stage.event_set.filter(start__gte=date_request) if
                            event.start and event.end]
            if valid_events:
                avg_stage = [round((event.end - event.start).total_seconds() / 60, 4) for event in valid_events]
                self.labels.append('"{}"'.format(stage.name))
                self.data.append('{{meta: "{}", value: "{}"}}'.format(stage.name, round(sum(avg_stage) / len(avg_stage), 2)))


class JobTimeGraph(GraphView):
    graph_title = "Job times graph"
    type = "job_time"
    units = "mins"

    def get_graph_data(self, slug):
        self.labels, self.data = [], []
        self.pipeline = get_object_or_404(Pipeline, slug=slug)
        date_request = datetime.now(tz=UTC) - timedelta(hours=self.time_object['hours'])
        for event in Event.objects.filter(stage__pipeline=self.pipeline, start__gte=date_request).distinct().values(
            'job_id'):
            self.labels.append('"{}"'.format(event["job_id"]))
            events = Event.objects.filter(
                job_id=event["job_id"], start__gte=date_request, end__isnull=False
            )
            avg_job_id = [
                round((event.end - event.start).total_seconds() / 60, 4) for event in events
            ]
            if avg_job_id:
                self.data.append(
                    '{{meta: "{}", value: "{}"}}'.format(event["job_id"], round(sum(avg_job_id) / len(avg_job_id), 2)))


class PipeStatusGraph(GraphView):
    graph_title = "Pipeline status graph"
    type = "pipe_status"
    units = ""

    def get_graph_data(self, slug):
        self.labels, self.data = [], []
        self.pipeline = get_object_or_404(Pipeline, slug=slug)
        date_request = datetime.now(tz=UTC) - timedelta(hours=self.time_object['hours'])
        for message in PipelineMessage.objects.filter(pipeline=self.pipeline,
                                                      timestamp__gte=date_request).values(
            'status').annotate(dcount=Count('status')):
            self.labels.append(f"'{message['status']}'")
            self.data.append(
                '{{meta: "{}", value: "{}"}}'.format(message["status"], message["dcount"]))


def error_404(request, exception):
    data = {'site_header': "CKiller Admin", 'has_permission': True}
    return render(request,'ckillers/404_killer.html', data)


def error_500(request):
    data = {'site_header': "CKiller Admin", 'has_permission': True}
    return render(request,'ckillers/500_killer.html', data)

import random

from datetime import datetime, timedelta

from django.core.files.storage import default_storage
from django.template.defaultfilters import slugify

from yaml import dump as yaml_dump


def unique_slug(value, model, slugfield="slug"):
    """Returns a slug on a name which is unique within a model's table

    This code suffers a race condition between when a unique
    slug is determined and when the object with that slug is saved.
    It's also not exactly database friendly if there is a high
    likelyhood of common slugs being attempted.

    A good usage pattern for this code would be to add a custom save()
    method to a model with a slug field along the lines of:

        def save(self):
            if not self.id:
                # replace self.name with your prepopulate_from field
                self.slug = unique_slug(self.name, self.__class__)
            super(self.__class__, self).save()

    Original pattern discussed at
    http://www.b-list.org/weblog/2006/11/02/django-tips-auto-populated-fields
    """
    suffix = 0
    slug = base = slugify(value)
    while model.objects.filter(**{slugfield: slug}).count() > 0:
        if suffix:
            slug = '-'.join([base, str(suffix)])

        # we hit a conflicting slug, so bump the suffix & try again
        suffix += 1
    return slug


def gen_datetime(min_year=1900, max_year=datetime.now().year):
    # generate a datetime in format yyyy-mm-dd hh:mm:ss.000000
    start = datetime(min_year, 1, 1, 00, 00, 00)
    years = max_year - min_year + 1
    end = start + timedelta(days=365 * years)
    return start + (end - start) * random.random()


def gen_50_dates():
    dates = []
    for i in range(0, 50):
        start = gen_datetime(2019, max_year=2019)
        end = start + timedelta(seconds=random.randint(30, 1000))
        dates.append({'start': start, 'end': end})
    return dates


def load_event_data():
    from ckillers.models import Event, Pipeline
    pipelines = Pipeline.objects.all()
    for pipeline in pipelines:
        dates = gen_50_dates()
        job_ids = [random.randint(14560, 158934) for _ in range(0,50)]
        for stage in pipeline.stage_set.all():
            for index, date in enumerate(dates):
                date['start'] += timedelta(seconds=random.randint(30,1000))
                date['end'] = date['start'] + timedelta(seconds=random.randint(30, 1000))
                Event.objects.create(stage=stage, start=date['start'], end=date['end'], job_id=job_ids[index])


def replace_file(file_name, content):
    if default_storage.exists(file_name):
        default_storage.delete(file_name)

    with default_storage.open(file_name, 'w') as pipeline_file:
        yaml_dump(content, stream=pipeline_file, allow_unicode=True)

from django.conf import settings
from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin
from django.core.files.storage import default_storage
from django.urls import path
from django.utils.translation import gettext_lazy as _
from django.http import HttpResponse, HttpResponseBadRequest

from django.urls import reverse
from django.utils.html import format_html

from .models import Client, Project, Pipeline, Stage, Event, PipelineMessage
from .forms import PipelinesForm, ClientCreationForm


class ClientsAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'timezone')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_display = ('username', 'email', 'user_groups', 'is_staff', 'token', 'timezone')
    add_form = ClientCreationForm

    def user_groups(self, obj):
        return ', '.join(obj.name for obj in obj.groups.all())

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(username=request.user.username)

    def save_model(self, request, obj, form, change):
        if request.user.is_superuser:
            super().save_model(request, obj, form, change)


class PipelinesAdmin(admin.ModelAdmin):
    form = PipelinesForm
    exclude = ('slug', 'uri')
    list_display = ('name', 'project', 'download_link', 'url', 'token')
    readonly_fields = ('download_link', 'url', 'token')
    search_fields = ['name', 'project__name']

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(project__client=request.user)

    def render_change_form(self, request, context, *args, **kwargs):
        if not request.user.is_superuser:
            context['adminform'].form.fields['project'].queryset = Project.objects.filter(client=request.user)
        return super().render_change_form(request, context, *args, **kwargs)

    def save_model(self, request, obj, form, change):
        obj.project.client = request.user

        stages_list_url = reverse('admin:ckillers_stage_changelist')
        if form.cleaned_data['pipeline_code']:
            extra_success_message = format_html(
                'Stages were also created. Please check the <a href="{}">Stages page</a>',
                stages_list_url)
            messages.info(request, extra_success_message)
        super().save_model(request, obj, form, change)

    def token(self, obj):
        return obj.project.client.token

    def url(self, obj):
        return f'{settings.API_URL}{obj.uri}'

    def download_link(self, obj):
        file_path = f'{settings.PIPELINES_ROOTDIR}/{obj.get_pipeline_file_name()}'
        if not default_storage.exists(file_path):
            return format_html('-')
        return format_html(
            '<a href="{}">Download pipeline</a>',
            reverse('admin:pipeline-file', args=[obj.get_pipeline_file_name()])
        )
    download_link.short_description = "Download pipeline"

    def download_file_view(self, request, file_name):
        file_path = f'{settings.PIPELINES_ROOTDIR}/{file_name}'
        response = HttpResponse(content_type='application/force-download')
        if default_storage.exists(file_path):
            response['Content-Disposition'] = f'attachment; filename="{file_name}"'
            with default_storage.open(file_path, 'r') as _file:
                pipeline_code = _file.read()
            response.write(pipeline_code)
            return response
        return HttpResponseBadRequest()

    def get_urls(self):
        urls = super().get_urls()
        urls += [
            path('files/<str:file_name>', self.download_file_view, name='pipeline-file'),
        ]
        return urls


class ProjectsAdmin(admin.ModelAdmin):
    exclude = ('slug', 'client')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(client=request.user)

    def save_model(self, request, obj, form, change):
        obj.client = request.user
        super().save_model(request, obj, form, change)


class StagesAdmin(admin.ModelAdmin):
    exclude = ('uri', 'slug')
    list_display = ('pipeline', 'name', 'slug', 'full_uri')
    readonly_fields = ('full_uri',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(pipeline__project__client=request.user)

    def render_change_form(self, request, context, *args, **kwargs):
        if not request.user.is_superuser:
            context['adminform'].form.fields['pipeline'].queryset = Pipeline.objects.filter(project__client=request.user)
        return super().render_change_form(request, context, *args, **kwargs)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.pipeline.project.client = request.user
        super().save_model(request, obj, form, change)

    def full_uri(self, obj):
        if int(obj.pipeline.type) not in settings.IGNORE_PIPELINE_CODE:
            env_var = settings.PIPELINE_IDS[obj._tool]
            code = '<pre><code>curl -X POST -H "TOKEN: <span onclick="showNice(this, \'{}\')">' \
                '<span class="label label-default">Show</span>"</span> \ <br>'
            code += '&nbsp;' * 5 + '{}{}/${}/</code></pre>'
            uri = format_html(
                code, obj.pipeline.project.client.token, settings.API_URL, obj.uri, env_var)
        else:
            code = '<pre><code>curl -X POST {}{}<span onclick="showNice(this, \'{}\',\'\')">' \
                '<span class="label label-default">Show</span></span>'
            uri = format_html(
                code, settings.API_URL, obj.uri, obj.pipeline.project.client.token)
        return uri

    class Media:
        js = ('ckillers/js/custom.js', )
    
    full_uri.short_description = format_html('Command')


class EventsAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(stage__pipeline__project__client=request.user)

    def render_change_form(self, request, context, *args, **kwargs):
        if not request.user.is_superuser:
            context['adminform'].form.fields['stage'].queryset = Stage.objects.filter(pipeline__project__client=request.user)
        return super().render_change_form(request, context, *args, **kwargs)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.stage.pipeline.project.client = request.user
        super().save_model(request, obj, form, change)


class PipelinesMessagesAdmin(admin.ModelAdmin):
    list_display = ('pipeline', 'status', 'message', 'job_id', 'timestamp')
    list_filter = ('pipeline__name', 'job_id')
    search_fields = ['job_id','pipeline__name']
    fields = list_display + ('metadata',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(pipeline__project__client=request.user)

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser:
            obj.pipeline.project.client = request.user
        super().save_model(request, obj, form, change)
    
    def has_add_permission(self, request):
        return False



admin.site.site_header = "Admin CKiller App"
admin.site.site_title = "CKiller Admin Portal"
admin.site.index_title = "Welcome to Ckiller Admin Portal"

admin.site.register(Client, admin_class=ClientsAdmin)
admin.site.register(Project, admin_class=ProjectsAdmin)
admin.site.register(Pipeline, admin_class=PipelinesAdmin)
admin.site.register(Stage, admin_class=StagesAdmin)
admin.site.register(Event, admin_class=EventsAdmin)
admin.site.register(PipelineMessage, admin_class=PipelinesMessagesAdmin)

import operator

from functools import reduce

from django.conf import settings


class DictHelper:
    """
    All methods assume safe keys access.
    """
    @staticmethod
    def nested_get(dictionary, *keys):
        return reduce(operator.getitem, keys, dictionary)

    @staticmethod
    def nested_append(dictionary, value, *keys):
        DictHelper.nested_get(dictionary, *keys[:-1])[keys[-1]].append(value)

    @staticmethod
    def nested_insert(dictionary, value, *keys):
        DictHelper.nested_get(dictionary, *keys[:-1])[keys[-1]].insert(0, value)


class GitlabCIParser:
    def __init__(self, pipeline_code, *args, **kwargs):
        self._raw_code = pipeline_code

        self.before_script_key = self._look_for_hook('before_script')
        self.after_script_key = self._look_for_hook('after_script')

        # If no stages are defined, then 'build', 'test' and 'deploy' are allowed.
        self.stages = pipeline_code.get('stages', ['build', 'test', 'deploy'])
        if not self.before_script_key or not self.after_script_key:
            self.job_names = self._extract_jobs()

    def _full_introspection(self, hook):
        potential_hooks = filter(
            lambda item: isinstance(item[1], dict) and item[0].startswith('.'), self._raw_code.items()
        )
        for key, item in potential_hooks:
            if hook in item:
                return '{}|{}'.format(key, hook)

    def _look_for_hook(self, hook):
        if hook in self._raw_code:
            return hook
        return self._full_introspection(hook)

    def _extract_jobs(self):
        """ Extract jobs from pipeline. """
        potential_jobs = filter(lambda item: isinstance(item[1], dict), self._raw_code.items())
        names = []
        for key, item in potential_jobs:
            stage = item.get('stage')
            if stage in self.stages:
                names.append(key)
        return names

    def build_curl_call(self, uri, token, action='start'):
        """ Build start/end curl calls. Stage.uri is expected. """
        return f'curl -H "TOKEN: {token}" -X POST -s {uri}/$CI_PIPELINE_ID/{action}'

    def parse(self, uris, token):
        """ Prepend start/end curl calls to _raw_code dict """
        for job, uri in uris.items():
            start = self.build_curl_call(settings.API_URL + uri, token, action='start')
            end = self.build_curl_call(settings.API_URL + uri, token, action='end')

            # if self.before_script_key:
            #     DictHelper.nested_insert(self._raw_code, start, *self.before_script_key.split('|'))

            # if self.after_script_key:
            #     DictHelper.nested_append(self._raw_code, end, *self.after_script_key.split('|'))

            has_script = 'script' in self._raw_code[job]
            if has_script and start not in self._raw_code[job]['script']:
                self._raw_code[job]['script'].insert(0, start)

            if has_script and end not in self._raw_code[job]['script']:
                self._raw_code[job]['script'].append(end)

        return self._raw_code

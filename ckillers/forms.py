from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group
from django.forms import ModelForm, ChoiceField, CharField, Textarea, ValidationError

import yaml

from .models import Pipeline


class PipelinesForm(ModelForm):
    type = ChoiceField(choices=settings.TYPES_CHOICES)
    pipeline_code = CharField(widget=Textarea, required=False)

    def clean_pipeline_code(self):
        if self.cleaned_data['pipeline_code']:
            try:
                self._pipeline_code = yaml.safe_load(self.cleaned_data['pipeline_code'])
            except yaml.scanner.ScannerError as invalid:
                raise ValidationError(invalid.problem)
        return self.cleaned_data['pipeline_code']

    def save(self, commit=True):
        instance = super().save(commit=commit)
        if hasattr(self, "_pipeline_code"):
            instance._pipeline_code = self._pipeline_code
        return instance

    class Meta:
        model = Pipeline
        fields = '__all__'


class ClientCreationForm(UserCreationForm):
    def save(self, commit=False):
        user = super().save(commit=True)
        user.is_staff = True
        clients_group = Group.objects.get(name='clients')
        user.groups.add(clients_group)
        return user

from django import template
from ckillers.views import belongs_to_clients_group

register = template.Library()
register.simple_tag(belongs_to_clients_group)

# Admin Killer

[![](https://images.microbadger.com/badges/image/manuel220/douwebserver.svg)](https://microbadger.com/images/manuel220/douwebserver "Get your own image badge on microbadger.com")

Admin app for managing resources of killer app.

## Getting Started

```sh
$ python3 -m venv project-env
$ source project-env/bin/activate
(project-env) $ pip install -r requirements/dev.txt
(project-env) $ python manage.py migrate
(project-env) $ python manage.py runserver
```

The following is only required if you want to connect to the production database.

```sh
(project-env) $ cp AdminKiller/{docker_settings.py,local_settings.py}
```

## Connecting with GCP sql instance

Choose and download the right versions accordingly to your operating system.

- Download Google Cloud SDK from [gcloud sdk]
- Download gcloud proxy from [gcloud proxy]

Before starting the proxy, you'll need to authenticate yourself using the gcloud cli.

```sh
$ gcloud auth application-default login
```

The previous command will open your default browser in order to let you authenticate yourself with
google. Credentials will be saved to file: `~/.config/gcloud/application_default_credentials.json`

Run the proxy
```
cloud_sql_proxy -instances="[INSTANCE_CONNECTION_NAME]"=tcp:["INSTANCE_PORT"]
```

`INSTANCE_CONNECTION_NAME` and `INSTANCE_PORT` can be found in [google cloud ckillerdb page]

Fill the databases section in the `local_settings.py` file

## Using Docker

When using docker, since this project uses meinheld + gunicorn, static files are not served from the container, instead, the variables are configured to point to the google cloud's bucket that we are using as CDN.

In case you want to update the static files on Google Cloud storage you would need to run the following commands locally:

```sh
python manage.py collectstatic

gsutil rsync -R static/ gs://douhackaton-storage/static
```

Now, you can proceed to build the image, if any particular variable is required, you can update file: `AdminKiller/docker_settings.py` as needed.

##### Build
```sh
docker build -t manuel220/douadmin . 
```

Then something similar to the command bellow, should start a container listening on port 7000:

##### Run
```sh
docker run -p 7000:80 --env DJANGO_SETTINGS_MODULE=AdminKiller.docker_settings --name admin -d manuel220/douadmin
```

From here you should be able to navigate to: http://localhost:7000 and login with admin/admin

If you want to run some django operations, you can do it directly from the container, for example if you want to clear the DB:

```sh
docker exec -it admin /bin/sh
# Once you are in the container:
rm ckillerdb
python manage.py migrate
python manage.py createsuperuser
```

[gcloud sdk]: https://cloud.google.com/sdk/docs/#install_the_latest_cloud_tools_version_cloudsdk_current_version
[gcloud proxy]: https://cloud.google.com/sql/docs/postgres/sql-proxy#install
[google cloud ckillerdb page]: https://console.cloud.google.com/sql/instances/ckillersdb/overview?project=just-function-258422&duration=PT1H
